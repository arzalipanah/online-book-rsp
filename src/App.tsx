import {React} from 'shared/utilities';
import AppRouter from "./AppRouter";

const App = () => <AppRouter/>

export default App;
