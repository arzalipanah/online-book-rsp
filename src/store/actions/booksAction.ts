import {actionTypes} from './types'
import {actionBuilder} from './actionBuilder'

export const errorLoadingHandler = actionBuilder(actionTypes.ERROR_LOADING_HANDLER, 'payload');
export const getBookListBegin = actionBuilder(actionTypes.BOOK_LIST_REQUESTED);
export const getBookListSuccess = actionBuilder(actionTypes.BOOK_LIST_SUCCESS, 'payload');
export const getBookListFailed = actionBuilder(actionTypes.BOOK_LIST_FAILED, 'payload');
