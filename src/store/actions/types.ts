export const actionTypes = {
    REQUEST_DID_CATCH: 'REQUEST_DID_CATCH',
    ERROR_LOADING_HANDLER: 'ERROR_LOADING_HANDLER',
    BOOK_LIST_REQUESTED: 'BOOK_LIST_REQUESTED',
    BOOK_LIST_SUCCESS: 'BOOK_LIST_SUCCESS',
    BOOK_LIST_FAILED: 'BOOK_LIST_FAILED',
}
