import {actionTypes} from './types'
import {actionBuilder} from './actionBuilder'

export const requestDidCatch = actionBuilder(actionTypes.REQUEST_DID_CATCH, 'payload')