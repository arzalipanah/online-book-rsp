import {all} from "redux-saga/effects";
import bookList  from "./booksSaga"

const sagaMiddlewares: Array<any> = [
    ...bookList,
];

export default function* combineSagaMiddleware() {
    yield all(sagaMiddlewares);
}
