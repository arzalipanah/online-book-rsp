import {call, put, takeLatest} from "shared/utilities";
import {errorLoadingHandler, getBookListFailed, getBookListSuccess} from "../actions";
import {actionTypes} from "../actions/types";
import DB from "db.json";

function* getBookList() {
    try {
        yield put(
            errorLoadingHandler({
                isLoading: true,
                hasError: false,
            })
        );

        const getBookListRequest = yield call(() => {
            return DB;
        });

        yield put(
            getBookListSuccess({
                list: getBookListRequest.books,
                isLoading: false,
            })
        );

    } catch (error) {
        yield put(
            getBookListFailed({
                hasError: true,
                isLoading: false,
                errorMessage: error.response.data.message,
            })
        );
        yield put(
            errorLoadingHandler({
                hasError: false,
            })
        );
    }
}


function* getBookListWatcher() {
    yield takeLatest(actionTypes.BOOK_LIST_REQUESTED, getBookList);
}

export default [
    getBookListWatcher(),
];