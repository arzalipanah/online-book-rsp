import {actionTypes} from "../actions/types";

export interface PayloadInterface {
    list: Array<any>,
    isLoading: false,
    hasError: false,
    errorMessage: null
}

export const initialState: PayloadInterface = {
    list: [],
    isLoading: false,
    hasError: false,
    errorMessage: null
}

const booksReducer = (state = initialState, action: any) => {
    let newState = state;

    switch (action.type) {
        case actionTypes.BOOK_LIST_SUCCESS:
            newState = {...state, ...action.payload};
            break;
        case actionTypes.BOOK_LIST_FAILED:
            newState = {...state, ...action.payload};
            break;
        case actionTypes.ERROR_LOADING_HANDLER:
            newState = {...state, ...action.payload};
            break;
        default:
            break;
    }
    return newState;
}

export default booksReducer