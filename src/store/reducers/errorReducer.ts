import {
    actionTypes
} from "store/actions/types";

export interface PayloadInterface {
    hasError: boolean
    errors: Array<any>
}

export const initialState: PayloadInterface = {
    hasError: false,
    errors: [],
}

const errorReducer = (state = initialState, action: any) => {
    let newState = state;
    switch (action.type) {
        case actionTypes.REQUEST_DID_CATCH:
            newState = {
                ...state,
                hasError: true,
                errors: [...state.errors, action.payload],
            };
        default:
            break;
    }
    return newState;
};
export default errorReducer;
