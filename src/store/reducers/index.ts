import {combineReducers} from 'redux'
import errorReducer from './errorReducer'
import booksReducer from './booksReducer'

export default combineReducers({
    errorReducer: errorReducer,
    books: booksReducer
})
