import {React, ReactDOM} from 'shared/utilities';
import * as serviceWorker from 'serviceWorker';
import {ErrorBoundary, Provider} from "shared/utilities";
import '_config';
import App from './App';
import store from "./store";

ReactDOM.render(
    <React.StrictMode>

        <ErrorBoundary>
            <Provider store={store}>
                <App/>
            </Provider>
        </ErrorBoundary>

    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
