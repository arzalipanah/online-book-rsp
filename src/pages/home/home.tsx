import {Layout, MyIcon, React} from "shared/utilities";
import Banner from 'assets/images/banner.png'
import Books from "shared/components/Hybrids/Books/Books";
import Categories from "../../shared/components/Hybrids/Categories/Categories";

const Home = () => {

    return <Layout>
        <MyIcon src={Banner} width={'100%'}/>
        <div className={'main'}>
            <Categories/>
            <div className={'d-flex flex-column'}>
                <Books/>
                <Books/>
                <Books/>
            </div>
        </div>
    </Layout>;

};

export default Home;
