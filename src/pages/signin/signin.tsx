import {Layout, React, MyInput, MyButton, useHistory} from "shared/utilities";

const Signin = () => {
        const history = useHistory();

        return (
            <Layout withHeader={false}>
                <div className={`d-flex flex-column justify-space-around full-height signin-page`}>
                    <div className={`d-flex flex-column justify-center signin-form`}>
                        <label>{'آدرس ایمیل'}</label>
                        <MyInput placeholder={''} onChange={() => console.log('')}/>
                        <label>{'رمز عبور (حداقل 6 حرف)'}</label>
                        <MyInput placeholder={''} onChange={() => console.log('')}/>
                        <MyButton onClick={() => history.push('/home')}>{'ورود'}</MyButton>
                    </div>
                </div>
            </Layout>
        );
    }
;

export default Signin;
