import {React} from "shared/utilities";

const NotFound = () => (<h1 className={`d-flex flex-column align-center justify-center full-height`}>NotFound</h1>
);

export default NotFound;
