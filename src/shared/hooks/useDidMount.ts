import { useEffect } from "shared/utilities";

type Func = () => void;
const useDidMount = (fn: Func) => {
  useEffect(() => {
    fn();
  }, []);
};

export default useDidMount;
