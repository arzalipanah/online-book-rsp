import {MyIcon, React} from "shared/utilities";
import Logo from 'assets/icons/logo.png'
import UserIcon from 'assets/icons/profile.svg'

const Header = () => {

    return (
        <div className={`d-flex wrap align-center justify-space-between header`}>
            <MyIcon src={Logo} width={50}/>
            <div className={`cursor-pointer`}>
                <MyIcon src={UserIcon} width={25}/>
                <span className={`text`}>{' ندا میراقایی'}</span>
            </div>
        </div>
    );
};

export default Header;