import {React, Header} from "shared/utilities";

interface LayoutInterface {
    children: any
    withHeader?: boolean
}

const Layout = ({children, withHeader = true, ...props}: LayoutInterface) => {
    return (
        <> {withHeader ?
            <>
                <Header/>{children}
            </>
            : <>{children}</>}
        </>
    );
};

export default Layout;
