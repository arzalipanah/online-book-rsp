import {React, useDidMount, useState} from "shared/utilities";
import api from "shared/utilities/constants/api";

const Categories = () => {
    const [categories, setCategories] = useState([] as Array<any>);

    useDidMount(() =>
        categories.length === 0 &&
        api.getData((data: any) => setCategories(data.category)));

    return <div className={'categories'}>
        <div className={'header regular-medium'}>{'دسته بندی'}</div>
        <ul className={'list'}>
            {
                categories.map((ct) => <li key={ct.id + 'ct'}>
                    <span>{ct?.title}</span>
                </li>)
            }
        </ul>
    </div>;

};

export default Categories;
