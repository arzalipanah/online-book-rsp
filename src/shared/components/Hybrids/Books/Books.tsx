import {MyCard, MyIcon, MyModal, React, useDidMount, useDispatch, useSelector, useState} from "shared/utilities";
import {getBookListBegin} from "store/actions";
import LazyLoad from "react-lazyload";

const Books = () => {
    const [openModal, setModal] = useState(false);
    const [modalInfo, setModalInfo] = useState({} as any);
    const isLoading = useSelector((state: any) => state.books.isLoading);
    const hasError = useSelector((state: any) => state.books.hasError);
    const data = useSelector((state: any) => state.books.list);

    const dispatch = useDispatch();
    useDidMount(() =>
        data.length === 0 &&
        dispatch(getBookListBegin()));

    const toggleModal = (item: any) => {
        setModalInfo(item);
        setModal((open) => !open);
    };

    const renderCards = () => {
        if (hasError) {
            return <h5>لیست کتاب ها دریافت نشد :(</h5>;
        }

        return (
            <div className={'wrapper'}>
                {data && data.map((item: any) => (
                    <LazyLoad key={item.id + 'bk'} offset={100} height={100}>
                        <MyCard
                            bookImg={item.image}
                            author={item.author}
                            title={item.title}
                            onCardClick={() => toggleModal(item)}
                        />
                    </LazyLoad>
                ))}
            </div>
        );
    };

    return <div className={'books'}>
        <MyModal open={openModal} onCLose={toggleModal}>
            <div className={'d-flex align-center info'}>
                <MyIcon src={modalInfo.image} width={180}/>
                <div>
                    <p>{`کتاب ${modalInfo.title} | ${modalInfo.author}`}</p>
                    <div className={'center'}>
                        <p>{` نویسنده: ${modalInfo.author}`}</p>
                        <p>{` مترجم: ${modalInfo.translator}`}</p>
                    </div>
                    <p>{` بخشی از کتاب: ${modalInfo.title}`}</p>
                    <p className={'summary'}>{modalInfo.summary}</p>
                </div>
            </div>
        </MyModal>
        {isLoading ? <div className={`loader`}/> : renderCards()}</div>;
};

export default Books;
