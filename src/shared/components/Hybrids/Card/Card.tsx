import {MyIcon, React} from "shared/utilities";

interface CardInterface {
    bookImg: string,
    title: string,
    link?: string,
    author: string
    onCardClick?: () => any
}

export const Card = ({
                         bookImg,
                         title,
                         link,
                         author,
                         onCardClick
                     }: CardInterface) => {
    return (
        <div className={`card`} onClick={onCardClick}>
            <MyIcon src={bookImg} alt="book"/>
            <div>
                <p>{title}</p>
                <span>{author}</span>
            </div>
        </div>
    );
};

export default Card;
