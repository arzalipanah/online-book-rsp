import {React} from "shared/utilities";

const Loading = () => {
    return (
        <h1 className={`d-flex flex-column align-center justify-center full-height text`}>در حال بارگزاری ...</h1>
    );
};

export default Loading;
