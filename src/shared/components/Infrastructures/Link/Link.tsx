import {React, Link, useHistory} from "shared/utilities";

interface LinkInterface {
    path: string
    children: any
}

export default ({path, children, ...props}: LinkInterface) => {
    const myPath = useHistory().location.pathname;

    return (
        <Link className={myPath === path ? "none-text-decoration activeLink link" : "none-text-decoration link"} to={path} {...props}>
            {children}
        </Link>
    );
};
