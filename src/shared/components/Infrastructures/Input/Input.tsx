import {React} from "shared/utilities";

interface InputInterface {
    className?: string
    onChange: (e: any) => void
    placeholder: string,
};

const Input = ({className, onChange, placeholder, ...rest}: InputInterface) => {
    return (
        <input
            className={`input ${className}`}
            placeholder={placeholder}
            onChange={onChange}
            {...rest}
        />
    );
};


export default Input;
