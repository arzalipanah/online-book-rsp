import {React} from "shared/utilities";

interface PrimaryButtonInterface {
    className?: string
    onClick?: (e: any) => void
    children?: any
}

const Button = ({
                    onClick,
                    children,
                    ...props
                }: PrimaryButtonInterface) => {
    return (
        <button
            className={'button cursor-pointer'}
            onClick={onClick}
            {...props}>{children}</button>
    );
};

export default Button;
