import {MyIcon, React} from "shared/utilities";
import CloseIcon from "assets/icons/close.svg"

interface ModalInterface {
    onCLose: (e: any) => void
    open: boolean
    children: any
};

const Modal = ({open, onCLose, children}: ModalInterface) => {
    return (
        <div className={'modal'} style={{display: open ? 'flex' : 'none'}}>
            <div className={'overlay'}/>
            <div className={'content'}>
                <MyIcon src={CloseIcon} width={15} className={'cursor-pointer close-btn'} onClick={onCLose}/>
                {children}
            </div>
        </div>
    );
};


export default Modal;
