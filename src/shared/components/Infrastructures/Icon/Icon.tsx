import  {React} from 'shared/utilities'

interface IConInterface {
    src: string
    alt?: string
    className?: string
    width?: string | number
    height?: number
    customStyle?: object
    onClick?: (e?: any) => void
}

export default (
    {
        src,
        alt,
        className = 'icon',
        width, height,
        customStyle,
        onClick
    }
        : IConInterface) => (
    <img src={src}
         alt={alt}
         className={className}
         width={width}
         height={height}
         style={{cursor: 'pointer', ...customStyle}}
         onClick={onClick}
    />
);