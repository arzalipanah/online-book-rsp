import React, {
    Component,
    createRef,
    Fragment,
    lazy,
    memo,
    PureComponent,
    Suspense,
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from "react";
import LazyLoad from "react-lazyload";
import {Provider, useDispatch, useSelector} from "react-redux";
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Link, Redirect, Route, Switch, useHistory} from "react-router-dom";
import {call, put, takeLatest} from "redux-saga/effects";

import ErrorBoundary from "shared/utilities/errorBoundary";
import useDidMount from "shared/hooks/useDidMount";
import isDevMode from "shared/utilities/constants/isDevMode";

import Loading from "shared/components/Hybrids/Loading";
import MyLink from "shared/components/Infrastructures/Link/Link";
import MyModal from "shared/components/Infrastructures/Modal/Modal";
import MyIcon from "shared/components/Infrastructures/Icon/Icon";
import MyInput from "shared/components/Infrastructures/Input/Input";
import MyButton from "shared/components/Infrastructures/Button/Button";
import MyCard from "shared/components/Hybrids/Card/Card";
import Layout from "shared/components/Hybrids/Layout/Layout";
import Header from "shared/components/Hybrids/Header/Header";

export {
    React,
    Component,
    useCallback,
    Fragment,
    PureComponent,
    useState,
    useEffect,
    createRef,
    useRef,
    useMemo,
    memo,
    Suspense,
    lazy,
    LazyLoad,
    Provider, useSelector, useDispatch,
    ReactDOM,
    Router, Route, Switch,
    Link, useHistory, Redirect,
    takeLatest, call, put,
    ErrorBoundary,
    isDevMode,
    useDidMount,
    Loading,
    MyLink,
    MyModal,
    MyIcon,
    MyInput,
    MyButton,
    MyCard,
    Layout,
    Header
};
