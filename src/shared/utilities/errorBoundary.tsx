import {React, PureComponent, isDevMode} from "shared/utilities";

interface ErrorBoundaryState {
    readonly error: any;
    readonly errorInfo: any;
}

class ErrorBoundary extends PureComponent<{}, ErrorBoundaryState> {
    constructor(props: any) {
        super(props);
        this.state = {errorInfo: null, error: null};
    }

    componentDidCatch(error: any, errorInfo: any) {
        this.setState({error: error, errorInfo: errorInfo});
    }

    render() {
        const {error, errorInfo} = this.state;
        if (errorInfo) {
            return (
                <>
                    {
                        isDevMode ? <>
                            <h2>Something went wrong.</h2>
                            <details style={{whiteSpace: "pre-wrap"}}>
                                {error && error.toString()}
                                <br/>
                                {errorInfo && errorInfo.componentStack}
                            </details>
                        </> : <></>
                    }
                </>
            );
        } else return this.props.children;
    }
}

export default ErrorBoundary;
