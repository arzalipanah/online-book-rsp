import {React, Suspense, lazy, Router, Route, Switch, Redirect} from "shared/utilities";
import Loading from "shared/components/Hybrids/Loading";

const lazyHome = lazy(() => import("pages/home/home"));
const lazySignIn = lazy(() => import("pages/signin/signin"));
const lazyNotFound = lazy(() => import("pages/notFound"));

const AppRouter = () => {
    return (
        <Router>
            <Suspense fallback={<Loading/>}>
                <Switch>
                    <Route
                        exact
                        path="/"
                        render={(props: any) => {
                            return (
                                <Redirect
                                    to={{pathname: "/signin"}}
                                />
                            );
                        }}
                    />
                    <Route exact={true} path="/signin" component={lazySignIn}/>
                    <Route path="/home" component={lazyHome}/>
                    <Route path="*" component={lazyNotFound}/>
                </Switch>
            </Suspense>
        </Router>
    );
};

export default AppRouter;
