# Online Book

This `React js` project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## structure

```
-- public/
---- ├── images
---- │       └── book1.png
---- │       └── book2.png
---- │       └── book3.png
---- │       └── book4.png
---- ├── favicon.ico
---- ├── manifest.json
---- ├── robots.txt

-- src/
---- ├── App.tsx
---- ├── _config
---- │   ├── index.ts
---- ├── assets
---- │   ├── css
---- │   │   └── abstract
---- │   │   ├──|── _generalClassNames.scss
---- │   │   ├──|── _mixins.scss
---- │   │   ├──|── _responsive.scss
---- │   │   ├──|── _variables.scss
---- │   │   └── base
---- │   │   ├──|── _reset.scss
---- │   │   ├──|── _typography.scss
---- │   │   └── App.scss
---- │   ├── font
---- │   │   ├── dana-bold.ttf
---- │   │   ├── dana-light.ttf
---- │   │   ├── dana-medium.ttf
---- │   │   └── dana-regular.ttf
---- │   │   └── dana-thin.ttf
---- │   └── icons
---- │       └── close.svg
---- │       └── logo.png
---- │       └── profile.svg
---- │   └── images
---- │       └── banner.png
---- ├── pages
---- │   ├── home
---- │   │   ├── home.tsx
---- │   │   └── _home.scss
---- │   ├── signin
---- │   │   ├── signin.tsx
---- │   │   └── signin.scss
---- │   ├── notFound
---- │   │   ├── index.tsx
---- ├── shared
---- │   ├── components
---- │   │   ├── Hybrids
---- │   │   │  ├── Book
---- │   │   │  |   |   ├── _book.scss
---- │   │   │  |   |   ├── Book.tsx
---- │   │   │  ├── Card
---- │   │   │  |   |   ├── _card.scss
---- │   │   │  |   |   ├── Card.tsx
---- │   │   │  ├── Categories
---- │   │   │  |   |   ├── _categories.scss
---- │   │   │  |   |   ├── Categories.tsx
---- │   │   │  ├── Header
---- │   │   │  |   |   ├── _header.scss
---- │   │   │  |   |   ├── Header.tsx
---- │   │   │  ├── Layout
---- │   │   │  |   |   ├── _layout.scss
---- │   │   │  |   |   ├── Layout.tsx
---- │   │   │  ├── Loading
---- │   │   │  |   |   ├── _loading.scss
---- │   │   │  |   |   ├── index.tsx
---- │   │   └── Infrastructures
---- │   │   │  |   ├── Icon
---- │   │   │  |   |   ├── Icon.tsx
---- │   │   │  |   ├── Input
---- │   │   │  |   |   ├── _input.scss
---- │   │   │  |   |   ├── Input.tsx
---- │   │   │  |   ├── Button
---- │   │   │  |   |   ├── _button.scss
---- │   │   │  |   |   ├── Button.tsx
---- │   │   │  |   ├── Link
---- │   │   │  |   |   ├── _link.scss
---- │   │   │  |   |   ├── Link.tsx
---- │   │   │  |   ├── Modal
---- │   │   │  |   |   ├── _modal.scss
---- │   │   │  |   |   ├── Modal.tsx
---- │   ├── hooks
---- │   │   └── useDidMount.ts
---- │   ├── utilities
---- │   │   ├── constants
---- │   │   │   ├── api.tsx
---- │   │   │   └── isDevMode.ts
---- │   │   ├── errorBoundary.tsx
---- │   │   ├── index.ts
---- ├── store
---- │   ├── action
---- │   │   ├── actionBulder.ts
---- │   │   └── errorActions.ts
---- │   │   └── index.ts
---- │   │   └── bookssAction.ts
---- │   │   └── types.ts
---- │   ├── reducers
---- │   │   ├── errorReducer.ts
---- │   │   └── index.ts
---- │   │   └── booksReducer.ts
---- │   ├── sagas
---- │   │   ├── index.ts
---- │   │   └── booksSaga.ts

```

root directories :

- _config : app custom and general config.
- assets : all assets including fonts, css, images, icons.
- Pages : Pages of application
- shared : utilities and components of app
- store : All redux structure including actions , reducers and types.

## Technologies

- `typescript`
- `react-lazyload`
- `redux` 
- `redux-saga` 
- `node-sass` 

## initiate the project :

`clone` project and after that install the depencencies. to achive that simply run the command below

```bash
npm install
```

or run

```bash
yarn
```

## start the project

to start development server need to run the start script :

```bash
npm run start
```

or

```bash
yarn start
```

and look for `http://localhost:3000` in your browser.

## build the project

to build the project you need to run build script :

```bash
npm run build
```

or

```bash
yarn build
```

and then you can serve it by installing `serve` package and serve the `build` directory.